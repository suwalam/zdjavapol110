package pl.sda.statement;

import pl.sda.util.JDBCUtil;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class JDBCStatementExample {

    public static void main(String[] args) {

        try {
            final Connection connection = JDBCUtil.getConnection();

            final Statement statement = connection.createStatement();


            String select = "SELECT * FROM person WHERE id = 1";

            final ResultSet resultSet = statement.executeQuery(select);

            while(resultSet.next()) {
                final int id = resultSet.getInt("id");
                final String firstName = resultSet.getString("first_name");
                final String lastName = resultSet.getString("last_name");
                final String pesel = resultSet.getString("pesel");

                System.out.println(id + " " + firstName + " " + lastName + " " + pesel);
            }

            connection.close();
            statement.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
