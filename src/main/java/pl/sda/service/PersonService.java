package pl.sda.service;

import com.mysql.cj.util.StringUtils;
import pl.sda.dao.PersonDao;
import pl.sda.model.Person;

public class PersonService {

    private final PersonDao personDao;

    public PersonService(PersonDao personDao) {
        this.personDao = personDao;
    }

    public Person getById(String id) {
        try {
            Integer parsedId = Integer.parseInt(id);
            return personDao.getById(parsedId);
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException("Bad argument: " + id);
        }
    }
}
