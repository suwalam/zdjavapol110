package pl.sda.callablestatement;

import pl.sda.util.JDBCUtil;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

public class JDBCCallableStatementExample {

    public static void main(String[] args) {

        try (Connection connection = JDBCUtil.getConnectionAlternative()) {

            String procedureCall = "{call selectAllFromPerson()}";

            final CallableStatement callableStatement = connection.prepareCall(procedureCall);

            final ResultSet resultSet = callableStatement.executeQuery();

            while(resultSet.next()) {
                final int id = resultSet.getInt("id");
                final String firstName = resultSet.getString("first_name");
                final String lastName = resultSet.getString("last_name");
                final String pesel = resultSet.getString("pesel");

                System.out.println(id + " " + firstName + " " + lastName + " " + pesel);
            }

            callableStatement.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
