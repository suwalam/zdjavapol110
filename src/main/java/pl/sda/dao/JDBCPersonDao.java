package pl.sda.dao;

import pl.sda.model.Person;
import pl.sda.util.JDBCUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class JDBCPersonDao implements PersonDao {

    @Override
    public Person getById(Integer id) {

        Person person = null;

        try {
            final Connection connection = JDBCUtil.getConnection();
            String selectById = "SELECT * FROM person WHERE id = ?";
            final PreparedStatement preparedStatement = connection.prepareStatement(selectById);
            preparedStatement.setInt(1, id);
            final ResultSet resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                final String firstName = resultSet.getString("first_name");
                final String lastName = resultSet.getString("last_name");
                final String pesel = resultSet.getString("pesel");
                person = new Person(id, firstName, lastName, pesel);
            }

        } catch (SQLException e) {
            return person;
        }

        return person;
    }

    @Override
    public List<Person> getAll() {




        return null;
    }
}
